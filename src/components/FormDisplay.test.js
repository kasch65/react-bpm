import React from 'react'
import ReactDOM from 'react-dom'
import { Provider, themes } from '@fluentui/react'
import FormDisplay from './FormDisplay'

const _mandators = [
	{ name: 'ARITHNEA', id: '4e32e63b-d8f7-43fb-b0e4-bbf8a407ff28' },
	{ name: 'adesso', id: 'cb3a488f-1d71-46be-b576-83e2c214c223' }
]

const _users = [
	{ firstName: 'Kai', lastName: 'Schäfer', mandatorId: '4e32e63b-d8f7-43fb-b0e4-bbf8a407ff28', id: '7cf20b44-17d8-4044-85a6-34917f0a6993', groupIds: ['1e01e6f2-d936-4baa-84cf-5b55e802a7de', 'ab0b9c92-6051-48c8-953e-bf6b38dd3315'] },
	{ firstName: 'Max', lastName: 'Headroom', mandatorId: '4e32e63b-d8f7-43fb-b0e4-bbf8a407ff28', id: '7cf20b44-17d8-4044-85a6-34917f0a6994', groupIds: ['1e01e6f2-d936-4baa-84cf-5b55e802a7de', 'ab0b9c92-6051-48c8-953e-bf6b38dd3315'] },
	{ firstName: 'Barbara', lastName: 'Behrens', mandatorId: 'cb3a488f-1d71-46be-b576-83e2c214c223', id: '3bfadd5a-33ac-4eb6-81b6-d2aab74fb536', groupIds: ['d35b72cc-bacd-4358-9e0c-c176b53c2930', '5bfd6064-4623-4519-974f-abf864f57233'] }
]

const _form = {
	'id': '250b147c-6717-40ca-bf9d-61be01266420',
	'rev': '5-3c247c67a7621fb7278617a658f37dd8',
	'name': 'person',
	'label': 'Persönliche Daten',
	'style': {},
	'elements': [
		{
			'label': 'First name',
			'name': 'firstName',
			'required': true,
			'inline': true,
			'value': 'X'
		},
		{
			'label': 'Last name',
			'name': 'lastName',
			'required': false,
			'inline': false
		},
		{
			'label': 'I agree to the Terms and Conditions',
			'name': 'tc',
			'control': {
				'as': 'input'
			},
			'type': 'checkbox',
			'required': true
		},
		{
			'type': 'group',
			'name': 'address',
			'label': 'Adresse',
			'style': {},
			'elements': [
				{
					'label': 'PLZ',
					'name': 'plz',
					'required': true,
					'inline': true,
					'value': '28195',
					'autofills': [
						{
							'field': 'ort'
						},
						{
							'field': 'strasse'
						}
					]
				},
				{
					'label': 'Ort',
					'name': 'ort',
					'required': true,
					'inline': false
				},
				{
					'label': 'Straße',
					'name': 'strasse',
					'required': true,
					'inline': false
				},
				{
					'label': 'Nummer',
					'name': 'hausnr',
					'required': true,
					'inline': false
				}
			]
		}
	],
	'mandatorId': '4e32e63b-d8f7-43fb-b0e4-bbf8a407ff28'
}

it('render UserSelection without crashing', () => {
	const div = document.createElement('div')
	ReactDOM.render(<Provider theme={themes.teams}>
		<FormDisplay data={{ form: _form, data: {formId: _form.id, values: {}}, preview: false }} hints={{}} mandator={_mandators[0]} user={_users[0]} />
	</Provider>, div)
	ReactDOM.unmountComponentAtNode(div)
})

import React from 'react'
import ReactDOM from 'react-dom'
import Table from './Table'

const testTableData = [
	{ name: 'n1', value: 'v1' },
	{ name: 'n2', value: 'v2' }
]

const testTableHeaders = [
	<Table.HeaderCell value="Name" key="name" />,
	<Table.HeaderCell value="Value" key="value" style={{ textAlign: 'center' }} />
]

const testTableContent = testTableData
	.map(f => <Table.TableRow key={f.name}
		cells={
			[
				<Table.TableCell value={f.name} key="name" />,
				<Table.TableCell value={f.value} key="value" style={{ textAlign: 'center' }} />
			]
		}
	/>)

it('renders without crashing', () => {
	const div = document.createElement('div')
	const table = <Table.Table tableHeaders={testTableHeaders} tableContent={testTableContent} />
	ReactDOM.render(table, div)
	ReactDOM.unmountComponentAtNode(div)
})

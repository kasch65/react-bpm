import React, { useState } from 'react'
import PeriodPicker from './PeriodPicker'
import Table from './Table'
import { Checkbox, Button, Form, Flex } from '@fluentui/react'

const Abrechnung = ({ rechnungen, kellner, tisch, rollen }) => {
	const [alleTische, setAlleTische] = useState(rollen.rolleKassierer || rollen.rolleBuchhaltung || rollen.rolleChef)
	const [alleKellner, setAlleKellner] = useState(true)
	const [alleKassierer, setAlleKassierer] = useState(rollen.rolleBuchhaltung || rollen.rolleChef)
	const [von, setVon] = useState(new Date((new Date().getTime() - 24 * 60 * 60 * 1000)))
	const [bis, setBis] = useState(new Date('2999-12-31T23:59:59.999'))
	const [expanded, setExpanded] = useState(false)

	const formStyle = {
		display: 'block'
	}

	const clickStyle = {
		cursor: 'pointer'
	}

	const thDivStyle = {
		textAlign: 'center'
	}

	/**
	 * See: https://gist.github.com/JamieMason/0566f8412af9fe6a1d470aa1e089a752
	 * @param {*} key Name of the gruping field as string
	 */
	/*const groupBy = key => array =>
		array.reduce((objectsByKeyValue, obj) => {
			const value = obj[key];
			objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
			return objectsByKeyValue;
		}, {});*/

	const _tableHeaders = [
		<Table.HeaderCell value="Datum" key="datum" />,
		<Table.HeaderCell value="Tisch" key="tisch" />,
		<Table.HeaderCell value="Kellner" key="kellner" />,
		<Table.HeaderCell value="Kassierer" key="kassierer" />,
		<Table.HeaderCell value="Prod." key="name" />,
		<Table.HeaderCell value={<div style={thDivStyle}>€</div>} key="€" />
	]

	const _tableContent = rechnungen
		.filter(r => new Date(r.rechnungsdatum).getTime() >= von.getTime())
		.filter(r => new Date(r.rechnungsdatum).getTime() < bis.getTime())
		.filter(r => r.tisch === tisch || alleTische)
		.filter(r => r.kassierer === kellner || alleKassierer)
		.filter(r => r.kellner === kellner || alleKellner)
		.map(r =>
			<Table.TableRow key={'abr_' + r.id} cells={
				[
					<Table.TableCell value={new Date(r.rechnungsdatum).toLocaleString()} key="datum" />,
					<Table.TableCell value={r.tisch} key="tisch" style={{ textAlign: 'center' }} />,
					<Table.TableCell value={r.kellner} key="kellner" />,
					<Table.TableCell value={r.kassierer} key="kassierer" />,
					<Table.TableCell value={r.name} key="name" />,
					<Table.TableCell value={r.preis.toFixed(2)} key="preis" style={{ textAlign: 'right' }} />
				]
			} />
		)

	_tableContent.push(
		<Table.TableRow key={'abr_datum'} cells={
			[
				<Table.TableCell value="Summe" key="lbl_summe" />,
				<Table.TableCell value="" key="tisch" />,
				<Table.TableCell value="" key="kellner" />,
				<Table.TableCell value="" key="kassierer" />,
				<Table.TableCell value="" key="name" />,
				<Table.TableCell value={
					rechnungen
						.filter(r => new Date(r.rechnungsdatum).getTime() >= von.getTime())
						.filter(r => new Date(r.rechnungsdatum).getTime() < bis.getTime())
						.filter(r => r.tisch === tisch || alleTische)
						.filter(r => r.kassierer === kellner || alleKassierer)
						.filter(r => r.kellner === kellner || alleKellner)
						.sort((a, b) => new Date(a.rechnungsdatum).getTime() - new Date(b.rechnungsdatum).getTime())
						.reduce((summe, r) => summe += r.preis, 0).toFixed(2)
				} key="summe" style={{ textAlign: 'right' }} />
			]
		} style={{ fontWeight: 'bold', height: '1.8rem' }} />
	)

	return (
		<Form id="abrechnung" style={formStyle}>
			<div>
				<h1 onClick={() => {
					if (!expanded) {
						setExpanded(oldValue => !oldValue)
					}
					else {
						setExpanded(oldValue => !oldValue)
					}
				}} style={clickStyle}>Abrechnung <Button circular icon={expanded ? 'chevron-down' : 'icon-chevron-end'} iconOnly loader="expand" /></h1>
				{
					expanded &&
					<>
						<Flex gap="gap.small" wrap>
							<Checkbox label="Alle Kellner" checked={alleKellner} onClick={() => setAlleKellner(oldValue => !oldValue)} />
							<Checkbox label="Alle Kassierer" checked={alleKassierer} onClick={() => setAlleKassierer(oldValue => !oldValue)} />
							<Checkbox label="Alle Tische" checked={alleTische} onClick={() => setAlleTische(oldValue => !oldValue)} />
						</Flex>
						<PeriodPicker von={von} bis={bis} setVon={setVon} setBis={setBis} />

						<Table.Table tableHeaders={_tableHeaders} tableContent={_tableContent} getRowKey={row => row[0][2]} />
					</>
				}
			</div>
		</Form>
	)

}

export default Abrechnung

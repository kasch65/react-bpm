import React, { useEffect } from 'react'
//import BpmnViewer from 'bpmn-js'
import BpmnModeler from 'bpmn-js/lib/Modeler'

import './BpmnDisplay.css'

const BpmnDisplay = ({ currentData, procXmls, theme }) => {

	useEffect(() => {
		if (currentData && currentData.processDef) {
			const processXml = procXmls.find(x => x.id === currentData.processDef.id)
			if (processXml && processXml.bpmn20Xml) {
				var modeler = new BpmnModeler({
					container: '#bpmn'
				})

				modeler.importXML(processXml.bpmn20Xml, function (err) {
					if (!err) {
						// console.log('success!')
						modeler.get('canvas').zoom('fit-viewport')

						const modeling = modeler.get('modeling')
						const elementRegistry = modeler.get('elementRegistry')

						let elementToColor
						if(currentData.task && currentData.task.taskDefinitionKey) {
							elementToColor = elementRegistry.get(currentData.task.taskDefinitionKey)
						}
						else {
							elementToColor = elementRegistry.getAll().find(e => e.type === 'bpmn:StartEvent')
						}

						if (elementToColor) {
							modeling.setColor([elementToColor], {
								fill: theme.siteVariables.colorScheme.default.backgroundActive1
							})
						}
					} else {
						console.error('BPMN rendering failed: ', err)
					}
				})
			}
		}
	}, [currentData, procXmls, theme])

	return <div style={{ maxWidth: '100%', height: '40rem', overflow: 'auto', margin: '1rem', border: 'solid 1px #eee', boxShadow: 'rgba(0, 0, 0, 0.1) 0px 0.2rem 0.4rem -0.075rem'}}>
		<div id="bpmn" style={{ width: '1000px', height: '1500px'}}></div>
	</div>

}
export default BpmnDisplay
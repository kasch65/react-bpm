import pouchBasics from './pouchBasics'
import PouchDB from 'pouchdb-browser'
const dbName = 'bpe-formdata'
let localDb = new PouchDB(dbName, { auto_compaction: true })

const sorters = []
let remoteDb

const setDbUri = remoteUri => {
	remoteDb = new PouchDB(remoteUri + '/' + dbName)
}

const addItem = item => pouchBasics.addItem(localDb, item)

const getItems = (selector, filter) => pouchBasics.getItems(localDb, sorters, selector, filter)

const getItem = id => pouchBasics.getItem(localDb, id)

const patchItem = (id, patch) => pouchBasics.patchItemAwait(localDb, id, patch)

const deleteItem = id => pouchBasics.deleteItem(localDb, id)

const onChange = (setItems, onAddItem, onPatchItem, onDeleteItem) => pouchBasics.onChange(localDb, sorters, setItems, onAddItem, onPatchItem, onDeleteItem)

const sync = (filter, filterParams) => pouchBasics.sync(localDb, remoteDb, filter, filterParams)

const stop = () => pouchBasics.stop(localDb)

export default { setDbUri, addItem, getItems, getItem, patchItem, deleteItem, onChange, sync, stop }
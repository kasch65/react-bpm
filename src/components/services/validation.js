import validateAddress from './validateAddress'

const _externalCheckers = [validateAddress.checkAddress]

// Calc initial hints for existing form
const calculateInputHints = (form_, input_, values_, inputHints_) => {
	return new Promise((resolve) => {
		//console.debug('_calculateInputHints', input_, values_, inputHints_)
		const value = values_[input_.name] || input_.value
		const missing = input_.required && !value
		const promises = []
		if (!inputHints_.formChecksStarted) {
			//console.debug('Requesting external checks...')
			inputHints_.formChecksStarted = true
			_externalCheckers.forEach(c => {
				const promise = c(form_, input_, values_, inputHints_)
				if (promise) {
					promises.push(promise)
				}
			})
		}
		else {
			//console.debug('Skipping external checks...')
		}
		if (missing) {
			promises.push(new Promise((resolve) => {
				console.warn(`Erforderliche Eingabe fehlt (${input_.name}, ${value})`)
				inputHints_[input_.name] = { error: `Erforderliche Eingabe fehlt (${input_.name}, ${value})` }
				return resolve()
			}))
		}
		else if (inputHints_[input_.name]) {
			if (inputHints_[input_.name].error) {
				promises.push(new Promise((resolve) => {
					console.info(`Eingabe jetzt OK (${input_.name}, ${value})`)
					delete inputHints_[input_.name].error
					return resolve()
				}))
			}
			if (inputHints_[input_.name].warning) {
				promises.push(new Promise((resolve) => {
					console.warn(`Eingabe jetzt OK (${input_.name}, ${value})`)
					delete inputHints_[input_.name].warning
					return resolve()
				}))
			}
		}

		Promise.all(promises)
			.then(() => resolve())
	})
}

const calculateGroupHints = (form_, group_, values_, collector_) => {
	return new Promise((resolve) => {
		//console.debug('_calculateGroupHints', group_, values_, collector_)
		const promises = []
		if (group_.elements) {
			group_.elements
				.filter(e => e.type === 'group')
				.forEach(e => {
					promises.push(calculateGroupHints(form_, e, values_, collector_))
				})
			group_.elements
				.filter(e => e.type !== 'group')
				.forEach(e => {
					promises.push(calculateInputHints(form_, e, values_, collector_))
				})
		}

		Promise.all(promises)
			.then(() => resolve())
	})
}

const calculateFormHints = async (form_, values_) => {
	console.debug('_calculateFormHints', form_, values_)
	const collector = {}
	await calculateGroupHints(form_, form_, values_, collector)
	return collector
}

export default { calculateInputHints, calculateGroupHints, calculateFormHints }
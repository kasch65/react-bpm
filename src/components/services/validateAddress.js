import axios from 'axios'

const _getValue = (item_, values_, inputName_) => {
	if (item_.name === inputName_) {
		let value = item_.value
		let input = values_[inputName_]
		if (input) {
			value = input
		}
		if (value === undefined) {
			// Right item but no value
			return null
		}
		else {
			return value
		}
	}
	else if (item_.elements) {
		for (let i = 0; i < item_.elements.length; ++i) {
			const e = item_.elements[i]
			const value = _getValue(e, values_, inputName_)
			if (value !== undefined) {
				// Item found
				return value
			}
		}
	}
	else {
		// Wrong item
		return undefined
	}
}

const _getAddressFilter = (form_, input_, values_) => {
	if (input_.name !== 'plz' && input_.name !== 'ort' && input_.name !== 'strasse') {
		return null
	}
	const zip = _getValue(form_, values_, 'plz')
	const city = _getValue(form_, values_, 'ort')
	const street = _getValue(form_, values_, 'strasse')
	if (!zip === undefined || !city === undefined || !street === undefined) {
		return null
	}
	const filter = {}
	let totalLength = 0
	if (zip && zip.length) {
		filter.zip = { zip: zip }
		totalLength += zip.length
	}
	if (city && city.length) {
		filter.city = { name: city }
		totalLength += city.length
	}
	if (street && street.length) {
		filter.street = { name: street }
		totalLength += street.length
	}
	if (totalLength >= 5) {
		return filter
	}
	else {
		return null
	}
}

const _getAddress = async (filter_, inputHints_) => {
	console.debug('Checking address service for: ', filter_)
	const response = await axios.post('https://addressservice.fairweben.de/list?pageSize=1000&page=1', filter_)
	console.debug('Address service response: ', response.data)
	if (response.data.length > 0) {
		const zipArray = []
		const cityArray = []
		const streetArray = []
		response.data.forEach(address => {
			if (!zipArray.includes(address.zip.zip)) {
				zipArray.push(address.zip.zip)
			}
			if (!cityArray.includes(address.city.name)) {
				cityArray.push(address.city.name)
			}
			if (!streetArray.includes(address.street.name)) {
				streetArray.push(address.street.name)
			}
		})
		zipArray.sort()
		cityArray.sort()
		streetArray.sort()
		inputHints_.plz = { choice: zipArray }
		inputHints_.ort = { choice: cityArray }
		inputHints_.strasse = { choice: streetArray }
	}
	else {
		inputHints_.plz = { warning: 'Adresse unbekannt' }
		inputHints_.ort = { warning: 'Adresse unbekannt' }
		inputHints_.strasse = { warning: 'Adresse unbekannt' }
	}
}

const checkAddress = async (form_, input_, values_, inputHints_) => {
	const filter = _getAddressFilter(form_, input_, values_)
	if (filter) {
		return await _getAddress(filter, inputHints_)
	}
	else {
		return null
	}
}

export default { checkAddress }
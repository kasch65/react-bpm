import Keycloak from 'keycloak-js'

const load = (config_, loginRequired_, onAuthenticated_) => {
	const keycloakTmp = Keycloak(config_)
	keycloakTmp.init({ onLoad: loginRequired_, promiseType: 'native' })
		.then(authenticated => {
			if (authenticated) {
				console.log('Authenticated.')
				const newUser = {
					id: keycloakTmp.tokenParsed.sub,
					name: keycloakTmp.tokenParsed.user_name,
					firstName: keycloakTmp.tokenParsed.given_name,
					lastName: keycloakTmp.tokenParsed.family_name,
					email: keycloakTmp.tokenParsed.email,
					application: keycloakTmp.tokenParsed.azp,
					roles: [],
					companies: []
				}
				for (let authority of keycloakTmp.tokenParsed.resource_access[newUser.application].roles) {
					if (authority.startsWith('ROLE_'))
						newUser.roles.push(authority)
					else if (authority.startsWith('COMPANY_'))
						newUser.companies.push(authority)
					else if (!newUser.mandator || !newUser.mandator.length)
						newUser.mandator = authority
				}
				onAuthenticated_(keycloakTmp, newUser)

			}
			else {
				onAuthenticated_(keycloakTmp, null)
			}
		})
}

const logout = keycloak_ => {
	if (keycloak_) {
		keycloak_.logout()
	}
}

const authAdmin = keycloak_ => {
	if (keycloak_) {
		window.open(`${keycloak_.authServerUrl}`, '_blank')
	}
}

const accountAdmin = keycloak_ => {
	if (keycloak_) {
		window.open(`${keycloak_.authServerUrl}/realms/${keycloak_.realm}/account`, '_blank')
	}
}

let refreshSchedule

const assertValidSession = keycloak_ => {
	return new Promise((resolve, reject) => {
		if (refreshSchedule) {
			clearInterval(refreshSchedule)
		}
		keycloak_.updateToken(30)
			.then(refreshed => {
				if (refreshed) {
					console.debug('Token was successfully refreshed')
				} else {
					console.debug('Token is still valid')
				}
				const token = keycloak_.token
				if (!token) {
					console.info('No current seesion. Login required')
					return reject({ err: 'noToken', msg: 'Keine aktuelle Sitzung. Login erforderlich.' })
				}
				else {
					// Reschedule
					refreshSchedule = setInterval(() => assertValidSession(keycloak_), 5 * 60 * 1000)
					return resolve(keycloak_.token)
				}
			})
			.catch(err => {
				console.error('Failed to refresh token!', err)
				return reject({ type: 'tokenExpired', msg: 'Token konnte nicht mehr erneuert werden!', err })
			})
	})
}


export default { load, logout, authAdmin, accountAdmin, assertValidSession }

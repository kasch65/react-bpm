import axios from 'axios'
import oAuth from './oAauth'

const _all = (keycloak_, method_, url_, config_, body_) =>
	new Promise((resolve, reject) => {
		let config = config_
		if (!config) {
			config = {}
		}
		if (!config.method) {
			config.method = method_
		}
		if (!config.url) {
			config.url = url_
		}
		if (!config.headers) {
			config.headers = {}
		}
		if (body_) {
			config.data = body_
		}
		oAuth.assertValidSession(keycloak_)
			.then(token => {
				config.headers.authorization = 'Bearer ' + token
				axios(config)
					.then(response => {
						if (!response) {
							console.error('No response from server!')
							return
						}
						resolve(response)
					})
					.catch(err => {
						console.error('REST call error: ', err)
						reject(err)
					})
			})
			.catch(err => {
				console.error('OAuth error: ', err)
				reject(err)
			})
	})

const get = (keycloak_, url_, config_) => _all(keycloak_, 'get', url_, config_)

const post = (keycloak_, url_, config_, body_) => _all(keycloak_, 'post', url_, config_, body_)

export default { get, post }
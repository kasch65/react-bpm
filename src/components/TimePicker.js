import React from 'react'
import { Input } from '@fluentui/react'

const TimePicker = ({ time, resetValue, onChange }) => {

	const _padNumber = (number, size) => {
		var s = '000000000' + number
		return s.substr(s.length - size)
	}

	return (
		<>
			{
				<>
					<Input
						type="date"
						value={`${_padNumber(time.getFullYear(), 4)}-${_padNumber(1 + time.getMonth(), 2)}-${_padNumber(time.getDate(), 2)}`}
						onChange={ev => {
							const d = ev.target.valueAsDate
							if (!d) {
								setTimeout(onChange(resetValue), 100)
								return
							}
							const hours = time.getUTCHours()
							const minutes = time.getUTCMinutes()
							const seconds = time.getUTCSeconds()
							d.setHours(hours, minutes, seconds)
							setTimeout(onChange(d), 100)
						}}></Input>
					<Input
						type="time"
						value={`${_padNumber(time.getHours(), 2)}:${_padNumber(time.getMinutes(), 2)}`}
						onChange={ev => {
							let t = ev.target.valueAsDate
							if (!t) {
								t = new Date(resetValue.getTime())
							}
							else {
								t.setHours(t.getUTCHours(), t.getUTCMinutes(), t.getUTCSeconds())
							}
							const hours = t.getHours()
							const minutes = t.getMinutes()
							const seconds = t.getSeconds()
							const clone = new Date(time.getTime())
							clone.setHours(hours, minutes, seconds)
							setTimeout(onChange(clone), 100)
						}}
					></Input>
				</>
			}
		</>
	)

}

export default TimePicker

import React from 'react'
import Table from './Table'
import { Icon } from '@fluentui/react'

const FormsTable = ({ forms, procDefs, selectedItem, onSelectBlankForm, onSelectTableItem }) => {

	const _getFormDisplayName = f => (f ? (f.label ? f.label : (f.name ? f.name : 'unbenannt')) : 'nix')

	const _formsTableHeaders = [
		<Table.HeaderCell value="" key="icon" />,
		<Table.HeaderCell value="Mandant" key="mandator" />,
		<Table.HeaderCell value="Prozess" key="proc" />,
		<Table.HeaderCell value="Rev" key="proc-rev" style={{ textAlign: 'center' }} />,
		<Table.HeaderCell value="Formular" key="name" />,
		<Table.HeaderCell value="Rev" key="form-rev" style={{ textAlign: 'center' }} />
	]

	const _formsTableContent = []
	procDefs.forEach(procDef => {
		const form = forms.find(f => procDef.startForm && f.id === procDef.startForm.key)
		if (form) {
			_formsTableContent.push(
				<Table.TableRow key={procDef.id}
					cells={
						[
							<Table.TableCell value={<Icon name="files-txt" />} key="icon" style={{ paddingBottom: '.4rem', paddingRight: '.5rem' }} />,
							<Table.TableCell value={procDef.tenantId ? procDef.tenantId : ''} key="mandator" />,
							<Table.TableCell value={procDef.name ? procDef.name : procDef.key} key="proc" />,
							<Table.TableCell value={procDef.version} key="proc-rev" style={{ textAlign: 'center' }} />,
							<Table.TableCell value={_getFormDisplayName(form)} key="name" />,
							<Table.TableCell value={Number(form.rev.split('-')[0])} key="form-rev" style={{ textAlign: 'center' }} />
						]
					}
					onClick={() => {
						console.log('Selected form: ', form)
						onSelectBlankForm(form, procDef)
						onSelectTableItem(procDef)
					}}
					selected={selectedItem === procDef} />
			)
		}
	})

	return <>
		<Table.Table tableHeaders={_formsTableHeaders} tableContent={_formsTableContent} getRowKey={row => row.key} />
	</>

}

export default { FormsTable }
import React from 'react'
import Table from './Table'

const UserInfo = ({ user, theme }) => {
	const _userTableContent = Object.entries(user)
		.map(e => <Table.TableRow key={e[0]}
			cells={
				[
					<Table.TableCell value={e[0] + ': '} key="name" style={{ fontWeight: 'bold' }} />,
					<Table.TableCell value={e[1]} key="value" style={{ cursor: 'text' }} />,
				]
			}
		/>)

	return (
		<div className="UserInfo" style={{ backgroundColor: theme.siteVariables.colorScheme.default.background2, padding: '1rem', cursor: 'default' }}>
			<h3>Account bearbeiten</h3>
			<Table.Table tableContent={_userTableContent} getRowKey={row => row.key} />
		</div>
	)

}
export default UserInfo
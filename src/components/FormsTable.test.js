import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from '@fluentui/react'
import FormsTable from './FormsTable'

const _procDefs = [
	{
		'_id': 'TwoSteps:2:8466392d-63ba-11ea-872a-0242ac110003',
		'_rev': '7-da842748236b62815722803d6cb7970e',
		'key': 'TwoSteps',
		'category': 'http://bpmn.io/schema/bpmn',
		'description': null,
		'name': 'Two Steps',
		'version': 2,
		'resource': 'TwoSteps.bpmn',
		'deploymentId': '846304db-63ba-11ea-872a-0242ac110003',
		'diagram': null,
		'suspended': false,
		'tenantId': null,
		'versionTag': null,
		'historyTimeToLive': null,
		'startableInTasklist': true,
		'startForm': {
			'key': '250b147c-6717-40ca-bf9d-61be01266420',
			'contextPath': null
		}
	}
]

const _forms = [
	{
		name: 'f1',
		rev: '1-a',
		elements: [
			{
				type: 'group',
				name: name,
				elements: [
					{
						name: 'firstName',
					}
				]
			}
		]
	},
	{
		name: 'f2',
		rev: '1-b',
		elements: [
			{
				type: 'group',
				name: name,
				elements: [
					{
						name: 'firstName',
					}
				]
			}
		]
	}
]


it('renders without crashing', () => {
	const div = document.createElement('div')
	const table = <Provider>
		<FormsTable.FormsTable procDefs={_procDefs} forms={_forms} selectedItem={_forms[0]} />
	</Provider>
	ReactDOM.render(table, div)
	ReactDOM.unmountComponentAtNode(div)
})

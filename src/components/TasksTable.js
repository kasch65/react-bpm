import React from 'react'
import Table from './Table'
import { Icon } from '@fluentui/react'

const getPermissions = (task, user) => {
	if (!task) {
		return {
			canRead: true,
			canLock: false,
			canEdit: true,
			canRelease: false,
			assignee: ''
		}
	}
	if (task.deleted || task.completed) {
		return {
			canRead: false,
			canLock: false,
			canEdit: false,
			canRelease: false,
			assignee: ''
		}
	}
	const assingnee = task.assignee
	const candidates = (task.identityLinks && task.identityLinks.filter(i => i.type === 'candidate')) || []
	return {
		canRead: candidates.length === 0
			|| (
				candidates
				&& candidates.length > 0
				&& candidates.find(c => (
					c.userId
					&& c.userId === user.name
				)
					|| (
						c.groupId
						&& user.roles.find(r => r === c.groupId)
					))
			),
		canLock: !assingnee,
		canEdit: assingnee && assingnee === user.name,
		canRelease: assingnee && assingnee === user.name,
		assignee: assingnee ? assingnee : ''
	}
}

const TasksTable = ({ formData, tasks, procDefs, forms, user, selectedItem, onSelectTask, onSelectTableItem }) => {

	const _getFormDisplayName = f => (f ? (f.label ? f.label : (f.name ? f.name : 'unbenannt')) : 'nix')

	const _inboxTableHeaders = [
		<Table.HeaderCell value="Status" key="state" />,
		<Table.HeaderCell value="Datum" key="datum" />,
		<Table.HeaderCell value="Rev" key="rev" style={{ textAlign: 'center' }} />,
		<Table.HeaderCell value="Mandant" key="mandator" />,
		<Table.HeaderCell value="Prozess" key="process" />,
		<Table.HeaderCell value="Rev" key="proc-rev" style={{ textAlign: 'center' }} />,
		<Table.HeaderCell value="Aufgabe" key="task" />,
		<Table.HeaderCell value="Formular" key="form" />
	]

	const _getStateIcon = (permissions) => {
		if (permissions.canEdit) {
			return <Icon name="edit" />
		}
		else if (permissions.canLock) {
			return <Icon name="files-empty" />
		}
		else {
			return <><Icon name="lock" />{permissions.assignee}</>
		}
	}

	const _getErrorIcon = (task) => {
		if (task.error) {
			return <Icon name="redbang" />
		}
		else {
			return null
		}
	}

	const _inboxTableContent = []
	tasks
		.filter(t => t.tenantId === user.mandator || !t.tenantId)
		.sort((a, b) => a.created.localeCompare(b.created))
		.forEach(task => {
			const fd = formData.find(t => t.id === task.id)
			const form = forms.find(f => f.id === task.formKey)
			const procDef = procDefs.find(f => f.id === task.processDefinitionId)
			const permissions = getPermissions(task, user)

			if (fd && form && procDef) {
				_inboxTableContent.push(<Table.TableRow key={fd.id}
					cells={
						[
							<Table.TableCell value={<>{_getStateIcon(permissions)}{_getErrorIcon(task)}</>} key="state" />,
							<Table.TableCell value={new Date(task.created).toLocaleString()} key="datum" />,
							<Table.TableCell value={Number(fd.rev.split('-')[0])} key="rev" style={{ textAlign: 'center' }} />,
							<Table.TableCell value={task.tenantId ? task.tenantId : ''} key="mandator" />,
							<Table.TableCell value={procDef.name ? procDef.name : procDef.key} key="process" />,
							<Table.TableCell value={procDef.version} key="proc-rev" style={{ textAlign: 'center' }} />,
							<Table.TableCell value={task.name ? task.name : task.taskDefinitionKey} key="task" />,
							<Table.TableCell value={_getFormDisplayName(form)} key="form" />,
						]
					}
					onClick={() => {
						onSelectTask(task, fd, form, procDef)
						onSelectTableItem(fd)
					}}
					selected={selectedItem === fd} />)
			}
		})

	return <Table.Table tableHeaders={_inboxTableHeaders} tableContent={_inboxTableContent} getRowKey={row => row.key} />

}

export default { getPermissions, TasksTable }
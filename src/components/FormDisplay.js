import React, { useState } from 'react'
import { Checkbox, Dropdown, Input, Button } from '@fluentui/react'
import TasksTable from './TasksTable'
import './FormDisplay.css'

const FormDisplay = ({ data, hints, user, isDirty, onInput, onChoice, onSubmit, onDelete, onLock, onRelease, onComplete }) => {
	const [dropdownOpen, setDropdownOpen] = useState(null)

	const _renderInput = (form_, input_, values_, hints_, preview_, onInput_) => {
		//console.debug('_renderInput = ', input_, data_, hints_, preview_, onInput_)
		if (input_.type === 'checkbox') {
			return <Checkbox
				name={input_.name}
				label={input_.label + (input_.required ? ' *' : '')}
				defaultChecked={values_[input_.name]}
				onChange={() => {
					values_[input_.name] = !values_[input_.name]
					onInput_(form_, input_, values_)
				}}
				disabled={preview_}
				error={hints_[input_.name] ? hints_[input_.name].error : null}
				warning={hints_[input_.name] ? hints_[input_.name].warning : null} />
		}
		else if (input_.type === 'select') {
			return (
				<div style={{ display: input_.inline ? 'grid' : 'block', overflow: 'visible' }} onClick={ev => ev.preventDefault()}>
					{
						input_.inline &&
						<label className="label ui-text" dir="auto">{input_.label}{input_.required ? ' *' : ''}</label>
					}
					<Dropdown fluid={true}
						items={input_.value}
						placeholder={`${input_.label ? input_.label : input_.name} wählen`}
						checkable
						defaultValue={
							values_[input_.name]
								? values_[input_.name]
								: null
						}
						getA11ySelectionMessage={{
							onAdd: item => {
								console.debug('Selection:', item)
								values_[input_.name] = item
								onChoice(form_, input_, values_)
							}
						}}
						onClick={() => {
							setDropdownOpen(old => old ? null : input_.name)
						}}
						onMouseEnter={() => setDropdownOpen(input_.name)}
						onMouseLeave={() => setDropdownOpen(null)}
						open={dropdownOpen === input_.name}
					/>
				</div>
			)
		}
		else {
			return (
				<div style={{ display: input_.inline ? 'grid' : 'block', overflow: 'visible' }}>
					{
						input_.inline &&
						<label className="label ui-text" dir="auto">{input_.label}{input_.required ? ' *' : ''}</label>
					}
					<Input
						name={input_.name}
						defaultValue={
							values_[input_.name]
								? values_[input_.name]
								: input_.value
						}
						onChange={event => {
							values_[input_.name] = event.target.value
							onInput_(form_, input_, values_)
						}}
						disabled={preview_}
						error={hints_[input_.name] ? hints_[input_.name].error : null}
						warning={hints_[input_.name] ? hints_[input_.name].warning : null}
						onFocus={() => setDropdownOpen(input_.name)} 
						fluid={Boolean(input_.fluid)}/>
					{
						hints_[input_.name] &&
						hints_[input_.name].choice &&
						hints_[input_.name].choice.length > 1 &&
						<Dropdown
							items={hints_[input_.name].choice}
							placeholder={`${input_.label ? input_.label : input_.name} wählen`}
							checkable
							getA11ySelectionMessage={{
								onAdd: item => {
									console.debug('Selection:', item)
									values_[input_.name] = item
									onChoice(form_, input_, values_)
								}
							}}
							onMouseEnter={() => setDropdownOpen(input_.name)}
							onMouseLeave={() => setDropdownOpen(null)}
							open={dropdownOpen === input_.name}
						/>
					}
				</div>
			)
		}
	}

	const _renderHeader = (content_, level_) => {
		const style = { marginTop: 0 }
		if (level_ === 1) {
			return <h1 style={style}>{content_}</h1>
		}
		if (level_ === 2) {
			return <h2 style={style}>{content_}</h2>
		}
		if (level_ === 3) {
			return <h3 style={style}>{content_}</h3>
		}
		if (level_ === 4) {
			return <h4 style={style}>{content_}</h4>
		}
		return <h5>{content_}</h5>
	}

	const _renderGroup = (form_, group_, values_, hints_, preview_, level_, onInput_) => {
		//console.debug('_renderGroup = ', form_, group_, data_, hints_, preview_, level_, onInput_)
		return <div key={group_.name} className="form-group" style={{ ...{maxWidth: '100%'}, ...group_.style }}>
			{
				group_.label &&
				_renderHeader(group_.label, level_)
			}
			<div style={{ display: 'flex', flexWrap: 'wrap' }}>
				<div style={{maxWidth: '100%'}}>
					<table style={{width: '100%'}}>
						<tbody>
							{
								group_.elements &&
								group_.elements
									.filter(e => e.type !== 'group')
									.map(e => <tr key={e.name}>
										<td colSpan={e.type === 'checkbox' ? 2 : 1}>
											{
												!e.inline &&
												e.type !== 'checkbox' &&
												<>
													{e.label}
													{e.required ? ' *' : ''}
												</>
											}
											{
												e.type === 'checkbox' &&
												_renderInput(form_, e, values_, hints_, preview_, onInput_)
											}
										</td>
										<td>
											{
												e.type !== 'checkbox' &&
												_renderInput(form_, e, values_, hints_, preview_, onInput_)
											}
										</td>
									</tr>
									)
							}
						</tbody>
					</table>
				</div>
				{
					group_.elements
						.filter(e => e.type === 'group')
						.map(g => _renderGroup(form_, g, values_, hints_, preview_, level_ + 1, onInput_))
				}
			</div>
		</div>
	}

	// console.debug('Rendering form with: ', data)

	const permissions = TasksTable.getPermissions(data.task, user)

	const _error = () => {
		if (data.task && data.task.error) {
			const meledung = data.task.error.statusText
			const url = data.task.error.url
			const code = data.task.error.status
			let text = ''
			const body = data.task.error.body
			if (body) {
				const bodyObj = JSON.parse(body)
				if (bodyObj && bodyObj.message) {
					text = bodyObj.message
				}
				else {
					text = JSON.stringify(body)
				}
			}
			return <div style={{ maxWidth: '100%', margin: '1rem', padding: '1rem', border: 'solid 1px #eee', boxShadow: 'rgba(255, 0, 0, 0.5) 0px 0.1rem 0.4rem -0.075rem' }}>
				<h3>Fehler im Geschäftsprozess</h3>
				<table style={{ width: '45rem' }}>
					<tbody>
						<tr><td><strong>Meldung:</strong></td><td>{meledung}</td></tr>
						<tr><td><strong>URL:</strong></td><td>{url}</td></tr>
						<tr><td><strong>Code:</strong></td><td>{code}</td></tr>
						<tr><td><strong>Text:</strong></td><td>{text}</td></tr>
					</tbody>
				</table>
			</div>
		}
		else {
			return null
		}
	}

	return (
		<>
			{_error()}
			<div style={{ maxWidth: '100%', margin: '1rem', padding: '1rem', border: 'solid 1px #eee', boxShadow: 'rgba(0, 0, 0, 0.1) 0px 0.2rem 0.4rem -0.075rem' }}>
				<form
					onSubmit={() => {
						console.debug('Submitting form: ', data)
						onSubmit(data)
					}}>
					<div className="form" disabled={!permissions.canEdit}>
						{
							_renderGroup(data.form, data.form, data.data.values, hints, !permissions.canEdit, 1, onInput)
						}
					</div>
					<div className="form-buttons" style={{ margin: '2rem 0 0' }}>
						{
							permissions.canLock &&
							<Button icon="edit" type="button" content="bearbeiten" primary onClick={() => {
								onLock(data)
							}} />
						}
						{
							(permissions.canEdit) &&
							<>
								<Button icon="accept" type="submit" content="speichern" primary disabled={!isDirty} />
							</>
						}
						{
							permissions.canRelease &&
							<>
								<Button icon="close" type="button" content="löschen" primary onClick={() => onDelete(data)} />
								<Button icon="files-empty" type="button" content="abgeben" primary disabled={isDirty} onClick={() => {
									onRelease(data)
								}} />
								<Button icon="accept" type="button" content="fertig" primary disabled={isDirty} onClick={() => {
									onComplete(data)
								}} />
							</>
						}
					</div>
				</form>
			</div>
		</>
	)
}

export default FormDisplay
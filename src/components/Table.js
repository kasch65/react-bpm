import React from 'react'
import './Table.css'

const borderColor = '#4440'

const HeaderCell = ({ value, style }) => {
	const cellStyle = {
		border: 'inherit',
		borderBottom: 'inherit',
		paddingRight: '.5rem'
	}
	const mergerdCellStyle = { ...cellStyle, ...style }

	return <td style={mergerdCellStyle}>{value}</td>
}

const TableRow = ({ cells, style, onClick, selected }) => {
	const rowStyle = {
		height: '2.5rem',
		border: 'none',
		borderBottom: `solid ${borderColor} 1px`,
		cursor: 'pointer'
	}
	const mergerdRowStyle = { ...rowStyle, ...style }

	return <tr style={mergerdRowStyle} onClick={() => onClick ? onClick() : null} className={selected ? 'selected' : ''}>{cells}</tr>
}

const TableCell = ({ value, style }) => {
	const cellStyle = {
		border: 'inherit',
		borderBottom: 'inherit',
		paddingRight: '.5rem'
	}
	const mergerdCellStyle = { ...cellStyle, ...style }

	return <td style={mergerdCellStyle}>{value}</td>
}


const Table = ({ tableHeaders, tableContent, style }) => {
	const tableStyle = {
		maxWidth: '100vw',
		borderCollapse: 'collapse'
	}
	const mergerdTableStyle = { ...tableStyle, ...style }

	const headerRowStyle = {
		fontWeight: 'bold',
		height: '1.5rem',
		border: 'none',
		borderBottom: `solid ${borderColor} 1px`
	}

	return (
		<div style={{ margin: '1rem 0' }}>
			<table style={mergerdTableStyle}>
				<thead>
					<tr style={headerRowStyle}>{tableHeaders}</tr>
				</thead>
				<tbody>{tableContent}</tbody>
			</table>
		</div>
	)
}

export default { Table, HeaderCell, TableRow, TableCell }
import React, { useState, useEffect } from 'react'
import { Icon, Divider, Button, Tooltip, Popup, Menu } from '@fluentui/react'

import * as _sampledata from '../data/sampledata.json'

import oAuth from './services/oAauth'
import axiosOAuth from './services/axiosOAuth'
import formsPouch from './services/formsPouch'
import camundaProcessDefPouch from './services/camundaProcessDefPouch'
import camundaProcessXmlPouch from './services/camundaProcessXmlPouch'
import camundaTasksPouch from './services/camundaTasksPouch'
import formDataPouch from './services/formDataPouch'
import validation from './services/validation'

import UserInfo from './UserInfo'
import FormDisplay from './FormDisplay'
import FormsTable from './FormsTable'
import TasksTable from './TasksTable'
import BpmnDisplay from './BpmnDisplay'

import './App.css'

const App = ({ theme }) => {

	const _mainStyle = {
		display: 'flex',
		flexWrap: 'wrap'
	}

	// Logged in user:
	const [keycloak, setKeycloak] = useState(null)
	const [user, setUser] = useState(null)
	// Login
	const [loginRequired, setLoginRequired] = useState('check-sso')
	// Lists
	const [forms, setForms] = useState(null)
	const [procDefs, setProcDefs] = useState(null)
	const [procXmls, setProcXmls] = useState(null)
	const [formData, setFormData] = useState(null)
	const [camundaTasks, setCamundaTasks] = useState(null)
	// Current form
	const [currentData, setCurrentData] = useState(null)
	const [originalData, setOriginalData] = useState(null)
	const [hints, setHints] = useState({})
	// Item highlight
	const [selectedItem, setSelectedItem] = useState(null)

	// Loag keycloak always when app is being loaded
	useEffect(() => {
		oAuth.load('/keycloak.json', loginRequired, (k, u) => {
			setUser(u)
			if (k) {
				setKeycloak(k)
			}
		})
	}, [loginRequired])

	// User has logged in
	useEffect(() => {

		const _onLogin = () => {
			formDataPouch.stop()
			camundaTasksPouch.stop()
			camundaProcessXmlPouch.stop()
			camundaProcessDefPouch.stop()
			formsPouch.stop()
			setCurrentData(null)
			formsPouch.sync('forms/byMandator', { mandator: user.mandator })
			// TODO: Filter... primary process definitions and tasks
			camundaProcessXmlPouch.sync('procxml/byMandator', { mandatorId: user.mandator })
			camundaProcessDefPouch.sync('procdef/byMandator', { mandatorId: user.mandator })
			const _filterPrams = { mandatorId: user.mandator, groupIds: user.roles.join(',') }
			//camundaTasksPouch.sync('task/byMandator', _filterPrams)
			console.debug('Task Filter: ', _filterPrams)
			camundaTasksPouch.sync('task/byMandatorAndGroups', _filterPrams)
			formDataPouch.sync('formdata/byMandatorAndGroups', _filterPrams)
		}

		const _onAuthenticated = (keycloak_, user_) => {
			setUser(user_)
			if (user_) {
				// Start Pouch sync
				axiosOAuth.get(keycloak_, '/db/couch')
					.then(response => {
						const couchUri = response.data.couchUri
						formsPouch.setDbUri(couchUri)
						camundaProcessXmlPouch.setDbUri(couchUri)
						camundaProcessDefPouch.setDbUri(couchUri)
						camundaTasksPouch.setDbUri(couchUri)
						formDataPouch.setDbUri(couchUri)

						formsPouch.onChange(setForms)
						camundaProcessXmlPouch.onChange(setProcXmls)
						camundaProcessDefPouch.onChange(setProcDefs)
						camundaTasksPouch.onChange(setCamundaTasks)
						formDataPouch.onChange(setFormData, null,
							patchedItem => {
								// Delete editor content if that document has been deleted
								setCurrentData(before => {
									setTimeout(() => setCurrentData(() => {
										if (before && before.data.id === patchedItem.id) {
											const clone = { ...before }
											clone.data = patchedItem
											return clone
										}
										else {
											return before
										}
									}), 0)
									return null
								})
							},
							deletedId => {
								// Delete editor content if that document has been deleted
								setCurrentData(old => {
									if (old && old.data.id === deletedId) {
										return null
									}
									else {
										return old
									}
								})
							})

						_onLogin(user_)
					})
					.catch(err => {
						console.log('Error: ', err)
					})

			}
		}

		if (keycloak && user) {
			_onAuthenticated(keycloak, user)
		}
	}, [keycloak, user])


	const _forceRerender = (setter, newValue) => {
		setter(null)
		setTimeout(() => setter(newValue), 0)
	}

	const _initDb = () => {
		console.debug('Initializing DB with: ', _sampledata.default)
		// Put some sample data into DB
		if (_sampledata && _sampledata.default) {
			_sampledata.default.forms.forEach(i => formsPouch.addItem(i)
				.then(newItem => console.debug('item added: ', newItem))
				.catch(err => console.debug('Failed to add item: ', err))
			)
			_sampledata.default.formdata.forEach(i => formDataPouch.addItem(i)
				.then(newItem => console.debug('item added: ', newItem))
				.catch(err => console.debug('Failed to add item: ', err))
			)
		}
	}

	const _onInput = (form_, input_, data_) => {
		const hintsClone = { ...hints }
		hintsClone.formChecksStarted = false
		validation.calculateInputHints(form_, input_, data_, hintsClone)
			.then(() => {
				console.debug('Updating hints:', hintsClone)
				const currentDataClone = { ...currentData }
				let valueChanges = false
				Object.entries(hintsClone).forEach(([key, value]) => {
					if (value.choice && value.choice.length === 1) {
						if (currentDataClone.data.values[key] !== value.choice[0]) {
							currentDataClone.data.values[key] = value.choice[0]
							valueChanges = true
						}
					}
				})
				setHints(hintsClone)
				if (valueChanges) {
					_forceRerender(setCurrentData, currentDataClone)
				}
			})
	}

	const _onChoice = (form_, input_, data_) => {
		const hintsClone = { ...hints }
		hintsClone.formChecksStarted = false
		validation.calculateInputHints(form_, input_, data_, hintsClone)
			.then(() => {
				console.debug('Updating hints:', hintsClone)
				const currentDataClone = { ...currentData }
				Object.entries(hintsClone).forEach(([key, value]) => {
					if (value.choice && value.choice.length === 1) {
						currentDataClone.data.values[key] = value.choice[0]
					}
				})
				setHints(hintsClone)
				_forceRerender(setCurrentData, currentDataClone)
			})
	}

	const _onFormSubmit = fd => {
		if (fd.data.id) {
			setOriginalData(JSON.parse(JSON.stringify(fd.data)))
			formDataPouch.patchItem(fd.data.id, fd.data)
		}
		else {
			formDataPouch.addItem(fd.data)
			setOriginalData(null)
			setCurrentData(null)
		}
	}

	const _onFormDelete = d => {
		setCurrentData(null)
		setSelectedItem(null)
		//formDataPouch.deleteItem(d.data.id)
		camundaTasksPouch.patchItem(d.task.id, { deleted: true })
	}

	const _onFormLock = fd => {
		camundaTasksPouch.patchItem(fd.task.id, { assignee: user.name })
	}

	const _onFormRelease = fd => {
		camundaTasksPouch.patchItem(fd.task.id, { assignee: null })
	}

	const _onFormComplete = fd => {
		setCurrentData(old => {
			const clone = { ...old }
			return clone
		})
		camundaTasksPouch.patchItem(fd.task.id, { assignee: null, completed: true })
	}

	const _onSelectBlankForm = (f, p) => {
		console.debug('Selected form: ', f)
		const newData = {
			form: f,
			data: {
				values: {},
				procDefId: p.id
			},
			processDef: p,
			mandator: p.tenantId
		}
		setOriginalData(JSON.parse(JSON.stringify(newData.data)))
		_forceRerender(setCurrentData, newData)
		setHints({})
		const formDiv = document.getElementById('doc')
		if (formDiv) {
			setTimeout(() => formDiv.scrollIntoView(true), 0)
		}
	}

	const _onSelectTask = async (t, fd, f, pd) => {
		console.debug('Selected Task: ', t)
		const newData = { form: f, task: t, data: fd, processDef: pd }
		setOriginalData(JSON.parse(JSON.stringify(fd)))
		_forceRerender(setCurrentData, newData)
		const formHints = await validation.calculateFormHints(newData.form, newData.data.values)
		// console.debug('New hints: ', formHints)
		setHints(formHints)
	}

	// console.debug('rendering App: ', currentData)

	const _getMenuButton = () => {
		if (user) {
			return <Popup
				content={<Menu vertical pointing="start" items={[
					{
						key: 'userinfo',
						content: <UserInfo user={user} theme={theme} />,
						onClick: () => oAuth.accountAdmin(keycloak)
					},
					{
						key: 'initDB',
						disabled: true,
						content: 'Datenbank initialisieren',
						onClick: _initDb
					},
					{
						key: 'user-admin',
						content: 'Administration',
						icon: {
							name: 'settings',
						},
						onClick: () => oAuth.authAdmin(keycloak)
					},
					{
						key: 'vpe-admin',
						content: 'Camunda BPE',
						icon: {
							name: 'settings',
						},
						onClick: () => window.open('https://bpe.fairweben.de/camunda/app/welcome/default/', '_blank')
					},
					{
						key: 'logout',
						content: 'Abmelden',
						icon: {
							name: 'lock',
						},
						onClick: () => oAuth.logout(keycloak)
					}
				]}></Menu>}
				trigger={<Button icon="presenter" iconPosition="after" content={user.firstName + ' ' + user.lastName} />}
			/>
		}
		else {
			return <div>
				<Button content="Anmelden/Registrieren" primary onClick={() => setLoginRequired('login-required')} />
				<Tooltip
					content="KeyCloak Benutzerverwaltung für Administratoren"
					trigger={
						<Button iconOnly><Icon name="settings" outline onClick={() => oAuth.authAdmin(keycloak)} /></Button>
					}
				/>
			</div>
		}
	}

	const _isDirty = () => {
		if (!currentData || !currentData.data || !currentData.data.values) {
			return false
		}
		if (!originalData || !originalData.values) {
			return false
		}
		let dirty = false
		Object.entries(currentData.data.values).filter(([key]) => key !== 'procDef').forEach(([key, value]) => {
			const otherValue = originalData.values[key]
			if (!dirty && otherValue !== value) {
				dirty = true
			}
		})
		if (!dirty) {
			Object.entries(originalData.values).filter(([key]) => key !== 'procDef').forEach(([key, value]) => {
				const otherValue = currentData.data.values[key]
				if (!dirty && otherValue !== value) {
					dirty = true
				}
			})
		}
		return dirty
	}

	return (
		<div id="app">
			<header style={{ display: 'flex', flexWrap: 'wrap', lineHeight: 1 }} color="brand">
				<div id="header-left" style={{ margin: '.2rem' }}><img src="arithnea.logo.jpg" alt="arithnea" style={{ height: '3rem' }} /></div>
				<div id="header-center" style={{ flexGrow: 1, textAlign: 'center' }}><h2 style={{ margin: '1.2rem' }}>Geschäftsprozesse automatisieren</h2></div>
				<div id="header-right" style={{ display: 'flex', margin: '.8rem' }}>
					{_getMenuButton()}
				</div>
			</header>
			<Divider />
			<main style={_mainStyle}>
				{
					user &&
					<>
						<div id="list" style={{ margin: '0 4rem 3rem .3rem' }}>
							<h2>Neue Formulare</h2>
							{
								forms && procDefs &&
								<FormsTable.FormsTable
									forms={forms.filter(f => f.mandator === user.mandator)}
									procDefs={procDefs}
									mandator={user.mandator}
									selectedItem={selectedItem}
									onSelectBlankForm={_onSelectBlankForm}
									onSelectTableItem={setSelectedItem} />
							}
							<h2>Aufgaben</h2>
							{
								formData && camundaTasks && procDefs && forms &&
								<TasksTable.TasksTable
									formData={formData}
									tasks={camundaTasks.filter(t => TasksTable.getPermissions(t, user).canRead)}
									procDefs={procDefs}
									forms={forms}
									user={user}
									selectedItem={selectedItem}
									onSelectTask={_onSelectTask}
									onSelectTableItem={setSelectedItem} />
							}
						</div>
						<div id="doc" style={{ maxWidth: '100%', overflow: 'hidden', margin: '0' }}>
							{
								currentData &&
								<>
									<FormDisplay
										data={currentData}
										hints={hints}
										user={user}
										isDirty={_isDirty()}
										onInput={_onInput}
										onChoice={_onChoice}
										onSubmit={_onFormSubmit}
										onDelete={_onFormDelete}
										onLock={_onFormLock}
										onRelease={_onFormRelease}
										onComplete={_onFormComplete} />
									<BpmnDisplay currentData={currentData} procXmls={procXmls} theme={theme} />
								</>
							}
						</div>
					</>
				}
				{
					!user &&
					<div id="welcome" style={{
						width: '100vw',
						height: '60vh',
						backgroundImage: 'url("new-data-services-Ar-iTL4QKl4-unsplash.webp")',
						backgroundSize: 'cover',
						backgroundPosition: 'center'
					}}></div>
				}
			</main>
			<Divider />
			<footer style={{ padding: '.2rem' }}>
				<strong>ARITHNEA GmbH</strong>
				{
					user &&
					<>
						&nbsp;&nbsp;&nbsp;Legende Icons:&nbsp;&nbsp;&nbsp;
						<span style={{ opacity: .66 }}>
							<Icon name="files-txt" /> Neues Formular&nbsp;&nbsp;&nbsp;<Icon name="files-empty" /> Ein Gruppenmitlied muss diese Aufgabe bearbeiten&nbsp;&nbsp;&nbsp;<Icon name="edit" /> Du bearbeitest diese Aufgabe&nbsp;&nbsp;&nbsp;<Icon name="lock" /> Jemand anders arbeitet daran
						</span>
					</>
				}
			</footer>
		</div>
	)

}

export default App

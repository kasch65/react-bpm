import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import { Provider, mergeThemes, themes } from '@fluentui/react'
import App from './components/App'
import * as serviceWorker from './serviceWorker'

const templateTheme = themes.teams

const prependTheme = {
	fontFaces: [
		{
			name: 'Segoe UI',
			//paths: ['/fonts/NotoSansJP-Light.otf'],
			//paths: ['/fonts/Ubuntu-Regular.ttf'],
			paths: ['/fonts/Roboto-Light.ttf'],
			//paths: ['/fonts/Roboto-Regular.ttf'],
			//paths: ['/fonts/segoeui-westeuropean/segoeui-regular.woff2'],
			props: {
				fontWeight: 400
			}
		},
		{
			name: 'Segoe UI',
			//paths: ['/fonts/NotoSansJP-Regular.otf'],
			//paths: ['/fonts/Ubuntu-Medium.ttf'],
			paths: ['/fonts/Roboto-Regular.ttf'],
			//paths: ['/fonts/Roboto-Medium.ttf'],
			//paths: ['/fonts/segoeui-westeuropean/segoeui-semibold.woff2'],
			props: {
				fontWeight: 600
			}
		},
		{
			name: 'Segoe UI',
			//paths: ['/fonts/NotoSansJP-Bold.otf'],
			//paths: ['/fonts/Ubuntu-Bold.ttf'],
			//paths: ['/fonts/Roboto-Medium.ttf'],
			paths: ['/fonts/Roboto-Bold.ttf'],
			//paths: ['/fonts/segoeui-westeuropean/segoeui-bold.woff2'],
			props: {
				fontWeight: 700
			}
		}
	]
}

const appTheme = {
	componentVariables: {
		Label: () => ({
			height: '2rem'
		})
	}
}

console.log('Themes: ', themes)
console.log('Theme: ', themes.teams)
const mergedTheme = mergeThemes(prependTheme, templateTheme, appTheme)
console.log('Merged theme: ', mergedTheme)

const iconKeys = Object.keys(mergedTheme.icons)
console.debug('Icons: ', iconKeys)

//ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(
	<Provider theme={mergedTheme}>
		<App theme={mergedTheme} />
	</Provider>,
	document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register()

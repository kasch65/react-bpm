require('dotenv').config()
const express = require('express')
const morgan = require('morgan')
const compression = require('compression')
const session = require('express-session')
const Keycloak = require('keycloak-connect')
const kcConfig = require('./public/keycloak.json')

console.log('Starting server...')

const couchUri = process.env.COUCH_URI || 'http://localhost:5984'

const app = express()

// compress all responses
app.use(compression())

//app.use(morgan('tiny'))
app.use(morgan((tokens, req, res) => {
	return [
		tokens.method(req, res),
		tokens.url(req, res),
		tokens.status(req, res),
		tokens.res(req, res, 'content-length'), '-',
		tokens['response-time'](req, res), 'ms',
		JSON.stringify(req.body)
	].join(' ')
}))

// Redirect http to https
/*app.use((req, res, next) => {
	if (req.header('x-forwarded-proto') && req.header('x-forwarded-proto') !== 'https') {
		const newLocation = `https://${req.header('host')}${req.url}`
		console.debug('redirecting to: ', newLocation)
		res.redirect(newLocation)
	} else {
		next()
	}
})*/

app.use(express.static('build'))


// Check authentification
app.use((req, res, next) => {
	if (!req.header('authorization')) {
		console.info('Inauthentic client')
		res.status(401).send({ info: 'Authorization header missing!' })
	} else {
		next()
	}
})

var memoryStore = new session.MemoryStore()
const keycloak = new Keycloak({ store: memoryStore }, kcConfig)
app.use(keycloak.middleware())
// URLs below this line are protected!

app.get('/db/couch', (req, res) => {
	res.send({couchUri: couchUri})
})

// Auf ungültige URLs reagieren
const unknownEndpoint = (request, response) => {
	response.status(404).send({ error: 'unknown endpoint' })
}

app.use(unknownEndpoint)

// Fehler an client senden
const errorHandler = (error, request, response, next) => {
	console.error('Error: ', error.message)

	if (error.name === 'CastError' && error.kind === 'ObjectId') {
		return response.status(400).send({ error: 'malformatted id' })
	}
	next(error)
}

app.use(errorHandler)

const PORT = process.env.PORT || 3003
app.listen(PORT, () => {
	console.log(`Server running on port ${PORT}`)
})
